package firstbot;

import java.util.Random;

import firstbot.macro.Economy;
import firstbot.macro.StrategyProfile;
import firstbot.map.MapUtils;
import firstbot.messaging.MessagingSystem;
import firstbot.messaging.MessagingSystem.ChannelType;
import firstbot.messaging.MessagingSystem.InvalidChannelIndexException;
import firstbot.navigation.Navigation;
import firstbot.task.TaskSystem;
import battlecode.common.Clock;
import battlecode.common.CommanderSkillType;
import battlecode.common.DependencyProgress;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import battlecode.common.TerrainTile;

public class AdvancedController implements RobotController {
	// Static data (not changed after first assignment)
	
	private RobotController controller;
	private Config config;
	private int spawnRound;
	
	private MapLocation hqLocation;
	
	private Random random;
	private MessagingSystem messagingSystem;
	private TaskSystem taskSystem;
	private StrategyProfile strategyProfile;
	private Economy economy;
	private Navigation navigation;
	
	// Dynamic data
	
	private Direction lastDirectionMoved = Direction.NONE;
	
	private RobotInfo[] cache_enemiesInRange;
	private RobotInfo[] cache_enemiesNearby;
	private RobotInfo[] cache_alliesInRange;
	private RobotInfo[] cache_alliesNearby;
	private MapLocation[] cache_enemyTowerLocations;
	
	private int expiration_enemiesInRange;
	private int expiration_enemiesNearby;
	private int expiration_alliesInRange;
	private int expiration_alliesNearby;
	private int expiration_enemyTowerLocations;
	
	private MapLocation enemyHQLocation;
	private boolean called_enemyHQLocation;
	
	public AdvancedController(RobotController controller) {
		this.controller = controller;
		config = Config.generate(this);
		spawnRound = Clock.getRoundNum();
		
		hqLocation = controller.senseHQLocation();
		
		random = new Random();
		messagingSystem = new MessagingSystem(this);
		taskSystem = new TaskSystem(this);
		
		strategyProfile = new StrategyProfile(this);
		
		if(controller.getType().isBuilding) {
			economy = new Economy(this);
		} else if(controller.getType() != RobotType.MISSILE) {
			navigation = new Navigation(this);
		}
	}
	
	public void nextRound() {		
		taskSystem.update(getType() != RobotType.MISSILE && getEnemiesInRange().length == 0);
		if(getType() == RobotType.HQ) {
			strategyProfile.centralizedUpdate();
			economy.centralizedUpdate();
		}
		if(getType().isBuilding) {
			economy.update();
		}
		strategyProfile.update();
	}

	public Config getConfig() {
		return config;
	}
	
	public MessagingSystem getMessagingSystem() {
		return messagingSystem;
	}
	
	public TaskSystem getTaskSystem() {
		return taskSystem;
	}
	
	public StrategyProfile getStrategyProfile() {
		return strategyProfile;
	}
	
	public Economy getEconomy() {
		return economy;
	}
	
	public Navigation getNavigation() {
		return navigation;
	}
	
	public Random getRandom() {
		return random;
	}

	public RobotInfo[] getEnemiesInRange() {
		if(Clock.getRoundNum() >= expiration_enemiesInRange) {
			cache_enemiesInRange = senseNearbyRobots(getType().attackRadiusSquared, getTeam().opponent());
			expiration_enemiesInRange = Clock.getRoundNum() + config.CACHE_LIFETIME;
		}
		return cache_enemiesInRange;
	}

	public RobotInfo[] getEnemiesNearby() {
		if(Clock.getRoundNum() >= expiration_enemiesNearby) {
			cache_enemiesNearby = senseNearbyRobots(config.NEARBY_DISTANCE, getTeam().opponent());
			expiration_enemiesNearby = Clock.getRoundNum() + config.CACHE_LIFETIME;
		}
		return cache_enemiesNearby;
	}
	
	public RobotInfo[] getAlliesInRange() {
		if(Clock.getRoundNum() >= expiration_alliesInRange) {
			cache_alliesInRange = senseNearbyRobots(getType().attackRadiusSquared, getTeam());
			expiration_alliesInRange = Clock.getRoundNum() + config.CACHE_LIFETIME;
		}
		return cache_alliesInRange;
	}
	
	public RobotInfo[] getAlliesNearby() {
		if(Clock.getRoundNum() >= expiration_alliesNearby) {
			cache_alliesNearby = senseNearbyRobots(config.NEARBY_DISTANCE, getTeam());
			expiration_alliesNearby = Clock.getRoundNum() + config.CACHE_LIFETIME;
		}
		return cache_alliesNearby;
	}
	
	public Direction getLastDirectionMoved() {
		return lastDirectionMoved;
	}
	
	public MapLocation getRallyPoint() {
		// TODO Handle multiple possible rally locations
		try {
			byte[] rawLocationBytes = MessagingSystem.toByteArray(messagingSystem.read(ChannelType.RALLY_LOCATION, 0));
			return MapUtils.locFromRelativeCoords(this, rawLocationBytes[2], rawLocationBytes[3]);
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
			return hqLocation;
		}
	}

	public int getAge() {
		return Clock.getRoundNum() - spawnRound;
	}

	/*
	 * Here goes the implementations of all RobotController methods
	 * We do this so that we can spy on when the certain functions are called  and use that knowledge
	 * f.eg. to evaluate the last direction moved
	 */
	
	public void addMatchObservation(String arg0) {
		controller.addMatchObservation(arg0);
	}
	
	public void attackLocation(MapLocation arg0) throws GameActionException {
		controller.attackLocation(arg0);
	}
	
	public void breakpoint() {
		controller.breakpoint();
	}
	
	public void broadcast(int arg0, int arg1) throws GameActionException {
		controller.broadcast(arg0, arg1);
	}
	
	public void build(Direction arg0, RobotType arg1) throws GameActionException {
		controller.build(arg0, arg1);
	}
	
	public boolean canAttackLocation(MapLocation arg0) {
		return controller.canAttackLocation(arg0);
	}
	
	public boolean canBuild(Direction arg0, RobotType arg1) {
		return controller.canBuild(arg0, arg1);
	}
	
	public boolean canLaunch(Direction arg0) {
		return controller.canLaunch(arg0);
	}
	
	public boolean canMine() {
		return controller.canMine();
	}
	
	public boolean canMove(Direction arg0) {
		return controller.canMove(arg0);
	}
	
	public boolean canSenseLocation(MapLocation arg0) {
		return controller.canSenseLocation(arg0);
	}
	
	public boolean canSpawn(Direction arg0, RobotType arg1) {
		return controller.canSpawn(arg0, arg1);
	}
	
	public void castFlash(MapLocation arg0) throws GameActionException {
		controller.castFlash(arg0);
		lastDirectionMoved = controller.getLocation().directionTo(arg0);
	}
	
	public DependencyProgress checkDependencyProgress(RobotType arg0) {
		return controller.checkDependencyProgress(arg0);
	}
	
	public void disintegrate() {
		controller.disintegrate();
	}
	
	public void explode() throws GameActionException {
		controller.explode();
	}
	
	public long getControlBits() {
		return controller.getControlBits();
	}
	
	public double getCoreDelay() {
		return controller.getCoreDelay();
	}
	
	public int getFlashCooldown() throws GameActionException {
		return controller.getFlashCooldown();
	}
	
	public double getHealth() {
		return controller.getHealth();
	}
	
	public int getID() {
		return controller.getID();
	}
	
	public MapLocation getLocation() {
		return controller.getLocation();
	}
	
	public int getMissileCount() {
		return controller.getMissileCount();
	}
	
	public double getSupplyLevel() {
		return controller.getSupplyLevel();
	}
	
	public Team getTeam() {
		return controller.getTeam();
	}
	
	public long[] getTeamMemory() {
		return controller.getTeamMemory();
	}
	
	public double getTeamOre() {
		return controller.getTeamOre();
	}
	
	public RobotType getType() {
		return controller.getType();
	}
	
	public double getWeaponDelay() {
		return controller.getWeaponDelay();
	}
	
	public int getXP() {
		return controller.getXP();
	}
	
	public boolean hasBuildRequirements(RobotType arg0) {
		return controller.hasBuildRequirements(arg0);
	}
	
	public boolean hasCommander() {
		return controller.hasCommander();
	}
	
	public boolean hasLearnedSkill(CommanderSkillType arg0) throws GameActionException {
		return controller.hasLearnedSkill(arg0);
	}
	
	public boolean hasSpawnRequirements(RobotType arg0) {
		return controller.hasSpawnRequirements(arg0);
	}
	
	public boolean isCoreReady() {
		return controller.isCoreReady();
	}
	
	public boolean isLocationOccupied(MapLocation arg0) throws GameActionException {
		return controller.isLocationOccupied(arg0);
	}
	
	public boolean isPathable(RobotType arg0, MapLocation arg1) {
		return controller.isPathable(arg0, arg1);
	}
	
	public boolean isWeaponReady() {
		return controller.isWeaponReady();
	}
	
	public void launchMissile(Direction arg0) throws GameActionException {
		controller.launchMissile(arg0);
	}
	
	public void mine() throws GameActionException {
		controller.mine();
	}
	
	public void move(Direction arg0) throws GameActionException {
		controller.move(arg0);
		lastDirectionMoved = arg0;
	}
	
	public int readBroadcast(int arg0) {
		try {
			return controller.readBroadcast(arg0);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public void resign() {
		controller.resign();
	}
	
	public MapLocation senseEnemyHQLocation() {
		if(!called_enemyHQLocation) {
			enemyHQLocation = controller.senseEnemyHQLocation();
			called_enemyHQLocation = true;
		}
		return enemyHQLocation;
	}
	
	public MapLocation[] senseEnemyTowerLocations() {
		if(Clock.getRoundNum() > expiration_enemyTowerLocations) {
			cache_enemyTowerLocations = controller.senseEnemyTowerLocations();
			expiration_enemyTowerLocations = Clock.getRoundNum() + config.CACHE_LIFETIME;
		}
		return cache_enemyTowerLocations;
	}
	
	public MapLocation senseHQLocation() {
		return hqLocation;
	}
	
	public RobotInfo[] senseNearbyRobots() {
		return controller.senseNearbyRobots();
	}
	
	public RobotInfo[] senseNearbyRobots(int arg0) {
		return controller.senseNearbyRobots(arg0);
	}
	
	public RobotInfo[] senseNearbyRobots(int arg0, Team arg1) {
		return controller.senseNearbyRobots(arg0, arg1);
	}
	
	public RobotInfo[] senseNearbyRobots(MapLocation arg0, int arg1, Team arg2) {
		return controller.senseNearbyRobots(arg0, arg1, arg2);
	}
	
	public double senseOre(MapLocation arg0) {
		return controller.senseOre(arg0);
	}
	
	public RobotInfo senseRobotAtLocation(MapLocation arg0) throws GameActionException {
		return controller.senseRobotAtLocation(arg0);
	}
	
	public TerrainTile senseTerrainTile(MapLocation arg0) {
		return controller.senseTerrainTile(arg0);
	}
	
	public MapLocation[] senseTowerLocations() {
		return controller.senseTowerLocations();
	}
	
	public void setIndicatorDot(MapLocation arg0, int arg1, int arg2, int arg3) {
		controller.setIndicatorDot(arg0, arg1, arg2, arg3);
	}
	
	public void setIndicatorLine(MapLocation arg0, MapLocation arg1, int arg2,
			int arg3, int arg4) {
		controller.setIndicatorLine(arg0, arg1, arg2, arg3, arg4);
	}

	public void setIndicatorString(int arg0, String arg1) {
		controller.setIndicatorString(arg0, arg1);
	}
	
	public void setTeamMemory(int arg0, long arg1) {
		controller.setTeamMemory(arg0, arg1);
	}

	public void setTeamMemory(int arg0, long arg1, long arg2) {
		controller.setTeamMemory(arg0, arg1, arg2);
	}
	
	public void spawn(Direction arg0, RobotType arg1) throws GameActionException {
		controller.spawn(arg0, arg1);
	}

	public void transferSupplies(int arg0, MapLocation arg1)
			throws GameActionException {
		controller.transferSupplies(arg0, arg1);
	}

	public void yield() {
		controller.yield();
	}

	@Override
	public boolean canSenseRobot(int arg0) {
		return controller.canSenseRobot(arg0);
	}

	@Override
	public RobotInfo senseRobot(int arg0) throws GameActionException {
		return controller.senseRobot(arg0);
	}
}
