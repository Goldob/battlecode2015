package firstbot;

import firstbot.core.BuildingCore;
import firstbot.core.ComputerCore;
import firstbot.core.LauncherCore;
import firstbot.core.MissileCore;
import firstbot.core.RobotCore;
import firstbot.core.UnitCore;
import firstbot.core.BuildingCore.AerospaceLabLogic;
import firstbot.core.BuildingCore.BarracksLogic;
import firstbot.core.BuildingCore.HQLogic;
import firstbot.core.BuildingCore.HandwashStationLogic;
import firstbot.core.BuildingCore.HelipadLogic;
import firstbot.core.BuildingCore.MinerFactoryLogic;
import firstbot.core.BuildingCore.SupplyDepotLogic;
import firstbot.core.BuildingCore.TankFactoryLogic;
import firstbot.core.BuildingCore.TechnologyInstituteLogic;
import firstbot.core.BuildingCore.TowerLogic;
import firstbot.core.BuildingCore.TrainingFieldLogic;
import battlecode.common.RobotController;

public class RobotPlayer {
	
	public static void run(RobotController rc) {	
		AdvancedController controller = new AdvancedController(rc);
		
		RobotCore robotCore = null;
		
		switch(controller.getType()) {
		case MISSILE:
			robotCore = new MissileCore(controller);
			break;
		case HQ:
			robotCore = new BuildingCore(controller, new HQLogic());
			break;
		case TOWER:
			robotCore = new BuildingCore(controller, new TowerLogic());
			break;
		case BEAVER:
		case MINER:
		case SOLDIER:
		case BASHER:
		case DRONE:
		case COMMANDER:
		case TANK:
			robotCore = new UnitCore(controller);
			break;
		case COMPUTER:
			robotCore = new ComputerCore(controller);
			break;
		case LAUNCHER:
			robotCore = new LauncherCore(controller);
			break;
		case AEROSPACELAB:
			robotCore = new BuildingCore(controller, new AerospaceLabLogic());
			break;
		case BARRACKS:
			robotCore = new BuildingCore(controller, new BarracksLogic());
			break;
		case HANDWASHSTATION:
			robotCore = new BuildingCore(controller, new HandwashStationLogic());
			break;
		case HELIPAD:
			robotCore = new BuildingCore(controller, new HelipadLogic());
			break;
		case MINERFACTORY:
			robotCore = new BuildingCore(controller, new MinerFactoryLogic());
			break;
		case SUPPLYDEPOT:
			robotCore = new BuildingCore(controller, new SupplyDepotLogic());
			break;
		case TANKFACTORY:
			robotCore = new BuildingCore(controller, new TankFactoryLogic());
			break;
		case TECHNOLOGYINSTITUTE:
			robotCore = new BuildingCore(controller, new TechnologyInstituteLogic());
			break;
		case TRAININGFIELD:
			robotCore = new BuildingCore(controller, new TrainingFieldLogic());
			break;
		}
		
		while(true) {
			robotCore.performRound(controller);
			controller.yield();
		}
	}
}