package firstbot.core;

import firstbot.AdvancedController;
import firstbot.core.logic.LogicModule;


public abstract class RobotCore {
	AdvancedController controller;
	
	public RobotCore(AdvancedController controller) {
		this.controller = controller;
	}
	
	public final void performRound(AdvancedController controller) {
		controller.nextRound();
		if(controller.getEnemiesInRange().length > 0) {
			getLogicModule().enemiesInRange(controller);
		} else if(controller.getEnemiesNearby().length > 0) {
			getLogicModule().enemiesNearby(controller);
		} else {
			getLogicModule().noEnemies(controller);
		}
	}
	
	public abstract LogicModule getLogicModule();
	
	protected AdvancedController getController() {
		return controller;
	}
}
