package firstbot.core;

import firstbot.AdvancedController;
import firstbot.core.logic.LogicModule;
import battlecode.common.GameActionException;
import static firstbot.core.logic.LogicUtils.alliesEngaged;
import static firstbot.core.logic.LogicUtils.battleLost;
import static firstbot.core.logic.LogicUtils.battleWon;
import static firstbot.core.logic.LogicUtils.canDoStuff;
import static firstbot.core.logic.LogicUtils.canShoot;
import static firstbot.core.logic.LogicUtils.cdBiggerThanRemainingCoreDelay;
import static firstbot.core.logic.LogicUtils.engage;
import static firstbot.core.logic.LogicUtils.ldBiggerThanRemainingWeaponDelay;
import static firstbot.core.logic.LogicUtils.nearDeath;
import static firstbot.core.logic.LogicUtils.pewPewPew;
import static firstbot.core.logic.LogicUtils.retreat;
import static firstbot.core.logic.LogicUtils.secureSupplies;
import static firstbot.core.logic.LogicUtils.toLocation;
import static firstbot.core.logic.LogicUtils.tryToMove;

public class UnitCore extends RobotCore {
	
	public UnitCore(AdvancedController controller) {
		super(controller);
	}

	@Override
	public LogicModule getLogicModule() {
		if(controller.getTaskSystem().taskAssigned()) {
			return BehaviorMode.TASK_EXECUTION.logic;
		} else if((controller.getLocation().distanceSquaredTo(controller.getRallyPoint()) > 100 && controller.getAlliesInRange().length < 10) || controller.getAlliesInRange().length < 3) {
			return BehaviorMode.REGROUP.logic;
		} else {
			switch(controller.getStrategyProfile().getCurrentStrategy()) {
			case ALL_IN:
				return BehaviorMode.STOORM.logic;
			case EARLY_ECON:
				if(controller.canMine()) {
					return BehaviorMode.WANDER_AND_MINE.logic;
				} /*else {
					// TODO
					return null;
				}*/
			case FORTIFY:
				if(controller.canMine()) {
					return BehaviorMode.WANDER_AND_MINE.logic;
				} /*else {
					// TODO
					return null;
				}*/
			case FULL_DRONES:
				if(controller.canMine()) {
					return BehaviorMode.WANDER_AND_MINE.logic;
				} /*else {
					// TODO
					return null;
				} */
			default:
				return BehaviorMode.REGROUP.logic; 
			}
		}
	}
	
	private static enum BehaviorMode {
		STOORM(new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				if(nearDeath(controller)) {
					secureSupplies(controller);
				}
				if(battleLost(controller)) {
					if(((canDoStuff(controller) && !retreat(controller, toLocation(controller, controller.getRallyPoint()))) 
							|| !cdBiggerThanRemainingCoreDelay(controller)) && canShoot(controller)) {
						 pewPewPew(controller, false);
					}
				} else {
					if(canShoot(controller)) {
						pewPewPew(controller, false);
					}
					if(canDoStuff(controller) && !ldBiggerThanRemainingWeaponDelay(controller)) {
						tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), false), false);
					}
				}
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				if(canDoStuff(controller))
					tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), false), battleLost(controller) || !(battleWon(controller) || alliesEngaged(controller)));
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				if(canDoStuff(controller))
					tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), false), false);
			}
		}), REGROUP(new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				if(nearDeath(controller)) {
					secureSupplies(controller);
				}
				if(!battleWon(controller)) {
					if(canDoStuff(controller)) {
						retreat(controller, toLocation(controller, controller.getRallyPoint()));
					}
					if(canShoot(controller) && !cdBiggerThanRemainingCoreDelay(controller)) {
						 pewPewPew(controller, false);
					}
				} else {
					if(canShoot(controller)) {
						pewPewPew(controller, false);
					}
					if(canDoStuff(controller) && !ldBiggerThanRemainingWeaponDelay(controller)) {
						tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), false), false);
					}
				}
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				if(canDoStuff(controller))
					tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), true), !(battleWon(controller)));
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				if(canDoStuff(controller))
					tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), true), false);
			}
			
		}), DEFENSE(new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				if(nearDeath(controller)) {
					secureSupplies(controller);
				}
				if(battleLost(controller)) {
					if(((canDoStuff(controller) && !retreat(controller, toLocation(controller, controller.getRallyPoint()))) 
							|| !cdBiggerThanRemainingCoreDelay(controller)) && canShoot(controller)) {
						 pewPewPew(controller, false);
					}
				} else {
					if(canShoot(controller)) {
						pewPewPew(controller, false);
					}
				}
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				if(!battleLost(controller)) {
					if(canDoStuff(controller) && alliesEngaged(controller)) {
						engage(controller);
					}
				}
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				if(canDoStuff(controller)&& controller.canMine()) {
					try {
						controller.mine();
					} catch (GameActionException e) {
						e.printStackTrace();
					}
				}
			}
		}), TASK_EXECUTION(new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				if(nearDeath(controller)) {
					secureSupplies(controller);
				}
				if(!battleWon(controller)) {
					if(((canDoStuff(controller) && !retreat(controller, toLocation(controller, controller.getRallyPoint()))) 
							|| !cdBiggerThanRemainingCoreDelay(controller)) && canShoot(controller)) {
						 pewPewPew(controller, false);
					}
				} else {
					if(canShoot(controller)) {
						pewPewPew(controller, false);
					}
				}
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				if(canDoStuff(controller)) {
					if(battleLost(controller)) {
						retreat(controller, toLocation(controller, controller.senseHQLocation()));
					} else if(battleWon(controller) && alliesEngaged(controller)) {
						engage(controller);
					} else if(controller.getTaskSystem().taskAssigned()) {
						if(!controller.getTaskSystem().getCurrentTask().perform(controller)) {
							if(controller.canMine()) {
								try {
									controller.mine();
								} catch (GameActionException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				if(controller.getTaskSystem().taskAssigned()) {
					if(!controller.getTaskSystem().getCurrentTask().perform(controller)) {
						if(canDoStuff(controller) && controller.canMine()) {
							try {
								controller.mine();
							} catch (GameActionException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}),
		WANDER_AND_MINE(new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				// TODO Auto-generated method stub
				
			}
		});
		
		BehaviorMode(LogicModule logic) {
			this.logic = logic;
		}
		
		private LogicModule logic;
	}
}
