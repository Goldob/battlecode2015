package firstbot.core.logic;

import firstbot.AdvancedController;

public interface LogicModule {
	
	public void enemiesInRange(AdvancedController controller);
	
	public void enemiesNearby(AdvancedController controller);
	
	public void noEnemies(AdvancedController controller);
}