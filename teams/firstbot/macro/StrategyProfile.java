package firstbot.macro;

import firstbot.AdvancedController;
import firstbot.map.MapUtils;
import firstbot.messaging.MessagingSystem;
import firstbot.messaging.MessagingSystem.ChannelType;
import firstbot.messaging.MessagingSystem.InvalidChannelIndexException;
import battlecode.common.Clock;
import battlecode.common.MapLocation;
import battlecode.common.RobotType;

// TODO Rewrite the class, or at least the strategy planning
public class StrategyProfile {
	private AdvancedController controller;
	private Strategy currentStrategy;
	
	public StrategyProfile(AdvancedController controller) {
		this.controller = controller;
	}
	
	// TODO ...
	public void centralizedUpdate() {
		if(Clock.getRoundNum() > 1700) {
			updateStrategy(Strategy.ALL_IN);
			setRallyPoint(controller.senseEnemyHQLocation());
		} else if(Clock.getRoundNum() >= 700 && controller.getEconomy().getRecentOreGainRatio() >= 15.0) {
			updateStrategy(Strategy.FULL_DRONES);
		} else {
			updateStrategy(Strategy.EARLY_ECON);
			setRallyPoint(new MapLocation((controller.senseHQLocation().x*2+controller.senseEnemyHQLocation().x)/3, (controller.senseHQLocation().y*2+controller.senseEnemyHQLocation().y)/3));
		}
	}
	
	public void update() {
		try {
			currentStrategy = Strategy.values()[controller.getMessagingSystem().read(ChannelType.STRATEGY, 0)];
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
		}
	}
	
	private void updateStrategy(Strategy newStrategy) {
		try {
			controller.getMessagingSystem().broadcast(newStrategy.ordinal(), ChannelType.STRATEGY, 0);
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
		}
	}
	
	public Strategy getCurrentStrategy() {
		return currentStrategy;
	}
	
	// TODO Handle multiple rally points
	private void setRallyPoint(MapLocation rallyPoint) {
		try {
			byte[] relativeCoords = MapUtils.locToRelativeCoords(controller, rallyPoint);
			controller.getMessagingSystem().broadcast(MessagingSystem.fromBytes((byte)0, (byte)0, relativeCoords[0], relativeCoords[1]), ChannelType.RALLY_LOCATION, 0);
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 *  TODO Work on those strategies & values; include supply depots (we don't use supplies yet, so we don't
	 *  need them)
	 */
	public static enum Strategy {
		/*ECONOMY, ATTACK, DEFEND, */
		EARLY_ECON {
			@Override
			public int getRobotCountLimit(RobotType type) {
				switch(type) {
				case BEAVER:
					return 15;
				case MINER:
					return 10;
				case MINERFACTORY:
					return 2;
				case SUPPLYDEPOT:
					//return 3;
				default:
					return 0;
				}
			}
		}, FORTIFY {
			@Override
			public int getRobotCountLimit(RobotType type) {
				switch(type) {
				case BEAVER:
					return 10;
				case MINER:
					return 15;
				case TANK:
					return 15;
				case SOLDIER:
					return 20;
				case BARRACKS:
					return 3;
				case TANKFACTORY:
					return 2;
				case MINERFACTORY:
					return 1;
				case SUPPLYDEPOT:
					//return 3;
				default:
					return 0;
				}
			}
		}, FULL_DRONES {
			@Override
			public int getRobotCountLimit(RobotType type) {
				switch(type) {
				case BEAVER:
					return 5;
				case MINER:
					return 10;
				case DRONE:
					return 30;
				case HELIPAD:
					return 3;
				case MINERFACTORY:
					return 1;
				case SUPPLYDEPOT:
					//return 3;
				default:
					return 0;
				}
			}
		}, ALL_IN {
			@Override
			public int getRobotCountLimit(RobotType type) {
				switch(type) {
				case BASHER:
					return 15;
				case BEAVER:
					return 20;
				case DRONE:
					return 20;
				case SOLDIER:
					return 15;
				case TANK:
					return 10;
				default:
					return 0;
				}
			}
		};
		
		public abstract int getRobotCountLimit(RobotType type);
	}
}
