package firstbot.messaging;

import firstbot.AdvancedController;
import battlecode.common.GameActionException;

public class MessagingSystem {
	private AdvancedController controller;
	
	public MessagingSystem(AdvancedController controller) {
		this.controller = controller;
	}
	
	public void broadcast(int data, ChannelType channelType, int subChannelIndex) throws InvalidChannelIndexException {
		try {
			controller.broadcast(channelType.getChannel(subChannelIndex), data);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}
	
	public int read(ChannelType channelType, int subChannelIndex) throws InvalidChannelIndexException {
		return controller.readBroadcast(channelType.getChannel(subChannelIndex));
	}
	
	public static byte[] toByteArray(int value) {
	    return new byte[] {
	        (byte) (value >> 24),
	        (byte) (value >> 16),
	        (byte) (value >> 8),
	        (byte) value};
	}

	public static int fromBytes(byte byte0, byte byte1, byte byte2, byte byte3) {
	     return ((byte0 << 24) & 0xff000000) | ((byte1 << 16) & 0x00ff0000) 
	    		 | ((byte2 << 8) & 0x0000ff00) | (byte3 & 0x000000ff);
	}
	
	// TODO
	public static enum ChannelType {
		// Serves mostly testing purposes
		EMPTY(0, 0),
		
		RESERVED_ORE(1,1),
		// 2000 channels needed (1 for each round)
		ORE_PROFILING(2,2001),
		
		STRATEGY(2002,2003),
		
		// Or maybe multiple locations?
		RALLY_LOCATION(2004,2004),
		
		NEXT_TASK_ID(2005,2005),
		// Each task uses a sub-channel of each of those
		TASK_DEFINITION(2006,3005),
		TASK_STATUS(3006,4005),
		
		ROBOT_COUNTS(4006, 4025),
		ROBOT_COUNT(4026, 4026);
		
		ChannelType(int first_index, int last_index) {
			this.first_index = first_index;
			this.last_index = last_index;
		}
		
		private int first_index, last_index;
		
		// TODO Throw an exception if wrong index
		public int getChannel(int index) throws InvalidChannelIndexException {
			if(index >= 0 && index < getSize()) {
				return first_index + index;
			} else {
				throw new InvalidChannelIndexException();
			}
		}
		
		public int getSize() {
			return Math.max(0, last_index-first_index+1);
		}
	}
	
	@SuppressWarnings("serial")
	public static class InvalidChannelIndexException extends Exception {}
}
