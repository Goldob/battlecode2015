package firstbot.navigation;

import firstbot.AdvancedController;
import firstbot.core.logic.LogicUtils;
import battlecode.common.Direction;
import battlecode.common.MapLocation;
import battlecode.common.RobotType;

public class Navigation {
	private AdvancedController controller;
	private NavigationMode navMode = NavigationMode.NOT_INITIALIZED;
	
	public Navigation(AdvancedController controller) {
		this.controller = controller;
		
		// TODO Change navigation mode depending on the unit type
		if(controller.getType() == RobotType.DRONE) {
			navMode = NavigationMode.NOCLIP;
		} else {
			navMode = NavigationMode.BUG_NAV;
		}
		navMode.navigator.init(controller);
	}
	
	public Direction navigate(MapLocation destination, boolean avoidEnemyHQ) {
		return navMode.navigator.navigate(controller, destination, avoidEnemyHQ);
	}
	
	public void setMode(NavigationMode navMode) {
		this.navMode = navMode;
	}
	
	public static enum NavigationMode {
		NOT_INITIALIZED(null),
		BUG_NAV(new Navigator() {
			private boolean turnRight;
			
			@Override
			public void init(AdvancedController controller) {
				turnRight = controller.getRandom().nextBoolean();
			}

			@Override
			public Direction navigate(AdvancedController controller, MapLocation destination, boolean avoidEnemyHQ) {
				Direction direction = LogicUtils.toLocation(controller, destination);
				for(int count = 0; count < 8; count++) {
					if(controller.canMove(direction)) {
						break;
					}
					direction = (turnRight ? direction.rotateRight() : direction.rotateLeft());
				}
				return direction;
			}
			
		}),
		NOCLIP(new Navigator() {

			@Override
			public void init(AdvancedController controller) {}

			@Override
			public Direction navigate(AdvancedController controller, MapLocation destination, boolean avoidEnemyHQ) {
				// TODO Maybe some more advanced stuff when we are near enemy HQ
				return LogicUtils.toLocation(controller, destination);
			}
			
		});
		
		NavigationMode(Navigator navigator) {
			this.navigator = navigator;
		}
		
		private Navigator navigator;
		
		private static interface Navigator {
			public void init(AdvancedController controller);
			
			public Direction navigate(AdvancedController controller, MapLocation destination, boolean avoidEnemyHQ);
		}
	}
}
