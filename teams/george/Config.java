package george;

import battlecode.common.Team;

// TODO Maybe methods instead of fields? Think about it...

public class Config {

	public int CACHE_LIFETIME = 1;
	
	public int RCOUNT_LOW_SUPPLIES = 10;
	public int NEARBY_DISTANCE = 48;
	
	// Micro related
	public int HEURESTIC_INITIAL_VALUE = 100;
	
	public int HEURESTIC_DPR_MULTIPLIER = 10;
	public int HEURESTIC_HEALTH_MULTIPLIER = 1;
	public int HEURESTIC_SUPPLY_MULTIPLIER = 5;
	
	public int HEURESTIC_HQ_BONUS = 150;
	public int HEURESTIC_TOWER_BONUS = 100;
	public int HEURESTIC_FUNCTIONAL_BONUS = 50;
	
	public static Config generate(AdvancedController controller) {
		Config config = new Config();
		
		if(readConfigValue(controller, "enabled", "false").equalsIgnoreCase("true")) {
			config.CACHE_LIFETIME = readTeamInteger(controller, "cache_lifetime", config.CACHE_LIFETIME);
			
			config.RCOUNT_LOW_SUPPLIES = readTeamInteger(controller, "rcount_low_supplies", config.RCOUNT_LOW_SUPPLIES);
			config.NEARBY_DISTANCE = readTeamInteger(controller, "nearby_distance", config.NEARBY_DISTANCE);
			
			config.HEURESTIC_INITIAL_VALUE = readTeamInteger(controller, "heurestic_initial_value", config.HEURESTIC_INITIAL_VALUE);
			
			config.HEURESTIC_DPR_MULTIPLIER = readTeamInteger(controller, "heurestic_dpr_multiplier", config.HEURESTIC_DPR_MULTIPLIER);
			config.HEURESTIC_HEALTH_MULTIPLIER = readTeamInteger(controller, "heurestic_health_multiplier", config.HEURESTIC_HEALTH_MULTIPLIER);
			config.HEURESTIC_SUPPLY_MULTIPLIER = readTeamInteger(controller, "heurestic_supply_multiplier", config.HEURESTIC_SUPPLY_MULTIPLIER);
			
			config.HEURESTIC_HQ_BONUS = readTeamInteger(controller, "heurestic_hq_bonus", config.HEURESTIC_HQ_BONUS);
			config.HEURESTIC_TOWER_BONUS = readTeamInteger(controller, "heurestic_tower_bonus", config.HEURESTIC_TOWER_BONUS);
			config.HEURESTIC_FUNCTIONAL_BONUS = readTeamInteger(controller, "heurestic_functional_bonus", config.HEURESTIC_FUNCTIONAL_BONUS);
		}
		
		return config;
	}
	
	private static String readConfigValue(AdvancedController controller, String name, String def) {
		return System.getProperty("bc.testing."+name, def);
	}
	
	private static String readTeamConfigValue(AdvancedController controller, String name, String def) {
		return readConfigValue(controller, "team-"+(controller.getTeam() == Team.A ? "a" : "b")+name, def);
	}
	
	private static int readTeamInteger(AdvancedController controller, String name, int def) {
		return Integer.valueOf(readTeamConfigValue(controller, name, String.valueOf(def)));
	}
}
