package george;

import george.core.BuildingCore;
import george.core.ComputerCore;
import george.core.LauncherCore;
import george.core.MissileCore;
import george.core.RobotCore;
import george.core.UnitCore;
import george.core.BuildingCore.AerospaceLabLogic;
import george.core.BuildingCore.BarracksLogic;
import george.core.BuildingCore.HQLogic;
import george.core.BuildingCore.HandwashStationLogic;
import george.core.BuildingCore.HelipadLogic;
import george.core.BuildingCore.MinerFactoryLogic;
import george.core.BuildingCore.SupplyDepotLogic;
import george.core.BuildingCore.TankFactoryLogic;
import george.core.BuildingCore.TechnologyInstituteLogic;
import george.core.BuildingCore.TowerLogic;
import george.core.BuildingCore.TrainingFieldLogic;
import battlecode.common.RobotController;

public class RobotPlayer {
	
	public static void run(RobotController rc) {	
		AdvancedController controller = new AdvancedController(rc);
		
		RobotCore robotCore = null;
		
		switch(controller.getType()) {
		case MISSILE:
			robotCore = new MissileCore(controller);
			break;
		case HQ:
			robotCore = new BuildingCore(controller, new HQLogic());
			break;
		case TOWER:
			robotCore = new BuildingCore(controller, new TowerLogic());
			break;
		case BEAVER:
		case MINER:
		case SOLDIER:
		case BASHER:
		case DRONE:
		case COMMANDER:
		case TANK:
			robotCore = new UnitCore(controller);
			break;
		case COMPUTER:
			robotCore = new ComputerCore(controller);
			break;
		case LAUNCHER:
			robotCore = new LauncherCore(controller);
			break;
		case AEROSPACELAB:
			robotCore = new BuildingCore(controller, new AerospaceLabLogic());
			break;
		case BARRACKS:
			robotCore = new BuildingCore(controller, new BarracksLogic());
			break;
		case HANDWASHSTATION:
			robotCore = new BuildingCore(controller, new HandwashStationLogic());
			break;
		case HELIPAD:
			robotCore = new BuildingCore(controller, new HelipadLogic());
			break;
		case MINERFACTORY:
			robotCore = new BuildingCore(controller, new MinerFactoryLogic());
			break;
		case SUPPLYDEPOT:
			robotCore = new BuildingCore(controller, new SupplyDepotLogic());
			break;
		case TANKFACTORY:
			robotCore = new BuildingCore(controller, new TankFactoryLogic());
			break;
		case TECHNOLOGYINSTITUTE:
			robotCore = new BuildingCore(controller, new TechnologyInstituteLogic());
			break;
		case TRAININGFIELD:
			robotCore = new BuildingCore(controller, new TrainingFieldLogic());
			break;
		}
		
		while(true) {
			robotCore.performRound(controller);
			controller.yield();
		}
	}
}