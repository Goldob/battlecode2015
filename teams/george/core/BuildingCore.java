package george.core;

import george.AdvancedController;
import george.StillTooMuchSwagException;
import george.core.logic.LogicModule;
import george.core.logic.LogicUtils;
import george.task.Task.TaskType;
import george.task.TaskSystem.TaskListener;
import battlecode.common.RobotType;
import static george.core.logic.LogicUtils.canDoStuff;
import static george.core.logic.LogicUtils.pewPewPew;
import static george.core.logic.LogicUtils.supplyNearbyUnits;
import static george.core.logic.LogicUtils.toEnemyHQ;
import static george.core.logic.LogicUtils.tryToSpawn;

public class BuildingCore extends RobotCore {
	private LogicModule logic;
	
	public BuildingCore(AdvancedController controller, BuildingLogic logic) {
		super(controller);
		this.logic = logic;
	}

	@Override
	public LogicModule getLogicModule() {
		return logic;
	}
	
	public static class HQLogic extends BuildingLogic {

		@Override
		public void logic(AdvancedController controller) {
			if(canDoStuff(controller) && controller.getEconomy().isDesirable(RobotType.BEAVER)) {
				tryToSpawn(controller, RobotType.BEAVER, toEnemyHQ(controller));
			}
			supplyNearbyUnits(controller);
		}

		@Override
		public void killBadGuys(AdvancedController controller) {
			pewPewPew(controller, controller.senseTowerLocations().length >= 5);
		}
		
	}
	
	public static class TowerLogic extends BuildingLogic {

		@Override
		public void logic(AdvancedController controller) {}

		@Override
		public void killBadGuys(AdvancedController controller) {
			pewPewPew(controller, false);
		}
	}

	public static class AerospaceLabLogic extends BuildingLogic {

		@Override
		public void logic(AdvancedController controller) {
			if(canDoStuff(controller) && controller.getEconomy().isDesirable(RobotType.LAUNCHER)) {
				tryToSpawn(controller, RobotType.LAUNCHER, toEnemyHQ(controller));
			}
		}

		@Override
		public void killBadGuys(AdvancedController controller) {}
	}

	public static class BarracksLogic extends BuildingLogic {

		@Override
		public void logic(AdvancedController controller) {
			if(LogicUtils.canDoStuff(controller)) {
				if(controller.getRandom().nextBoolean()) {
					if(controller.getEconomy().isDesirable(RobotType.SOLDIER)) {
						tryToSpawn(controller, RobotType.SOLDIER, toEnemyHQ(controller));
					} else if(controller.getEconomy().isDesirable(RobotType.BASHER)) {
						tryToSpawn(controller, RobotType.BASHER, toEnemyHQ(controller));
					}
				} else {
					if(controller.getEconomy().isDesirable(RobotType.BASHER)) {
						tryToSpawn(controller, RobotType.BASHER, toEnemyHQ(controller));
					} else if(controller.getEconomy().isDesirable(RobotType.SOLDIER)) {
						tryToSpawn(controller, RobotType.SOLDIER, toEnemyHQ(controller));
					}
				}
			}		
		}

		@Override
		public void killBadGuys(AdvancedController controller) {}
	}

	public static class HandwashStationLogic extends BuildingLogic {
		boolean swagAnnounced = false;
		boolean cleaningScheduled = false;
		
		@Override
		public void logic(final AdvancedController controller) {
			if(!swagAnnounced) {
				try {
					announceSWAG();
				} catch (StillTooMuchSwagException e) {
					e.printStackTrace();
				}	
			}
			if(!cleaningScheduled) {
				controller.getTaskSystem().scheduleTask(TaskType.WASH_HANDS, (byte)0, controller.getLocation(), new TaskListener() {
					@Override
					public void onSuccess() {
						cleaningScheduled = false;
					}

					@Override
					public void onFailure() {
						cleaningScheduled = false;
					}
				});
				cleaningScheduled = true;
			}
			
			controller.getTaskSystem().scanForDeadClaimersAndFailTasks();
		}

		@Override
		public void killBadGuys(AdvancedController controller) {}
		
		private void announceSWAG() throws StillTooMuchSwagException {
			swagAnnounced = true;
			throw new StillTooMuchSwagException();
		}
	}

	public static class HelipadLogic extends BuildingLogic {

		@Override
		public void logic(AdvancedController controller) {
			if(canDoStuff(controller) && controller.getEconomy().isDesirable(RobotType.DRONE)) {
				tryToSpawn(controller, RobotType.DRONE, toEnemyHQ(controller));
			}
		}

		@Override
		public void killBadGuys(AdvancedController controller) {}
	}

	public static class MinerFactoryLogic extends BuildingLogic {

		@Override
		public void logic(AdvancedController controller) {
			if(canDoStuff(controller) && controller.getEconomy().isDesirable(RobotType.MINER)) {
				tryToSpawn(controller, RobotType.MINER, toEnemyHQ(controller));
			}
		}

		@Override
		public void killBadGuys(AdvancedController controller) {}
	}

	public static class SupplyDepotLogic extends BuildingLogic {

		@Override
		public void logic(AdvancedController controller) {}

		@Override
		public void killBadGuys(AdvancedController controller) {}
	}

	public static class TankFactoryLogic extends BuildingLogic {

		@Override
		public void logic(AdvancedController controller) {
			if(canDoStuff(controller) && controller.getEconomy().isDesirable(RobotType.TANK)) {
				tryToSpawn(controller, RobotType.TANK, toEnemyHQ(controller));
			}
		}

		@Override
		public void killBadGuys(AdvancedController controller) {}
	}

	public static class TechnologyInstituteLogic extends BuildingLogic {

		@Override
		public void logic(AdvancedController controller) {
			if(canDoStuff(controller) && controller.getEconomy().isDesirable(RobotType.COMPUTER)) {
				tryToSpawn(controller, RobotType.COMPUTER, toEnemyHQ(controller));
			}
		}

		@Override
		public void killBadGuys(AdvancedController controller) {}
	}

	public static class TrainingFieldLogic extends BuildingLogic {

		@Override
		public void logic(AdvancedController controller) {
			if(canDoStuff(controller) && controller.getEconomy().isDesirable(RobotType.COMMANDER)) {
				tryToSpawn(controller, RobotType.COMMANDER, toEnemyHQ(controller));
			}
		}

		@Override
		public void killBadGuys(AdvancedController controller) {}
	}
	
	public static abstract class BuildingLogic implements LogicModule {
		
		public abstract void logic(AdvancedController controller);
		
		public abstract void killBadGuys(AdvancedController controller);
		
		@Override
		public final void enemiesInRange(AdvancedController controller) {
			// First shoot, then ask questions
			if(canDoStuff(controller)) {
				killBadGuys(controller);
			}
			logic(controller);
		}

		@Override
		public final void enemiesNearby(AdvancedController controller) {
			if(controller.getTaskSystem().taskAssigned()) {
				controller.getTaskSystem().getCurrentTask().perform(controller);
			}
			logic(controller);
		}

		@Override
		public final void noEnemies(AdvancedController controller) {
			if(controller.getTaskSystem().taskAssigned()) {
				controller.getTaskSystem().getCurrentTask().perform(controller);
			}
			logic(controller);	
		}
	}
}
