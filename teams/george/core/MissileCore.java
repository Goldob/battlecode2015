package george.core;

import george.AdvancedController;
import george.core.logic.LogicModule;
import battlecode.common.GameActionException;
import static george.core.logic.LogicUtils.canDoStuff;
import static george.core.logic.LogicUtils.closestRobot;
import static george.core.logic.LogicUtils.nearDeath;
import static george.core.logic.LogicUtils.toLocation;
import static george.core.logic.LogicUtils.tryToMove;

public class MissileCore extends RobotCore {
	private LogicModule logic;
	
	public MissileCore(AdvancedController controller) {
		super(controller);
		logic = new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				if(shouldDetonate(controller)) {
					try {
						controller.explode();
					} catch (GameActionException e) {
						e.printStackTrace();
					}
				} else if(nearDeath(controller) || controller.getAge() == 4) {
					controller.disintegrate();
				} else if(canDoStuff(controller)) {
					tryToMove(controller, controller.getLastDirectionMoved(), false);
				}
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				if(nearDeath(controller) || controller.getAge() == 4) {
					controller.disintegrate();
				} else if(canDoStuff(controller)) {
					tryToMove(controller, toLocation(controller, closestRobot(controller, controller.getEnemiesNearby()).location), false);
				}
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				if(canDoStuff(controller))
					tryToMove(controller, controller.getLastDirectionMoved(), false);
			}	
		};
	}
	
	private boolean shouldDetonate(AdvancedController controller) {
		// TODO It may not be the best way to handle this; we should estimate each unit's value first
		return controller.getEnemiesInRange().length > controller.getAlliesInRange().length;
	}

	@Override
	public LogicModule getLogicModule() {
		return logic;
	}

}
