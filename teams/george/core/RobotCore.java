package george.core;

import static george.core.logic.LogicUtils.nearDeath;
import static george.core.logic.LogicUtils.secureSupplies;
import static george.core.logic.LogicUtils.shareSupplies;
import george.AdvancedController;
import george.core.logic.LogicModule;


public abstract class RobotCore {
	AdvancedController controller;
	
	public RobotCore(AdvancedController controller) {
		this.controller = controller;
	}
	
	public final void performRound(AdvancedController controller) {
		controller.nextRound();
		if(controller.getEnemiesInRange().length > 0) {
			if(nearDeath(controller)) {
				secureSupplies(controller);
			}
			getLogicModule().enemiesInRange(controller);
		} else if(controller.getEnemiesNearby().length > 0) {
			getLogicModule().enemiesNearby(controller);
		} else {
			getLogicModule().noEnemies(controller);
		}
		shareSupplies(controller);
	}
	
	public abstract LogicModule getLogicModule();
	
	protected AdvancedController getController() {
		return controller;
	}
}
