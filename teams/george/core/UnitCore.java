package george.core;

import george.AdvancedController;
import george.core.logic.LogicModule;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotType;
import static george.core.logic.LogicUtils.alliesEngaged;
import static george.core.logic.LogicUtils.battleLost;
import static george.core.logic.LogicUtils.battleWon;
import static george.core.logic.LogicUtils.canDoStuff;
import static george.core.logic.LogicUtils.canShoot;
import static george.core.logic.LogicUtils.cdBiggerThanRemainingCoreDelay;
import static george.core.logic.LogicUtils.engage;
import static george.core.logic.LogicUtils.ldBiggerThanRemainingWeaponDelay;
import static george.core.logic.LogicUtils.nearDeath;
import static george.core.logic.LogicUtils.pewPewPew;
import static george.core.logic.LogicUtils.retreat;
import static george.core.logic.LogicUtils.secureSupplies;
import static george.core.logic.LogicUtils.toLocation;
import static george.core.logic.LogicUtils.tryToMove;

public class UnitCore extends RobotCore {
	private final static int MAX_RALLY_POINT_DISTANCE = 20;
	private final static int MAX_WANDER_DISTANCE  = 2000;
	
	public UnitCore(AdvancedController controller) {
		super(controller);
	}

	@Override
	public LogicModule getLogicModule() {
		if(controller.getTaskSystem().taskAssigned()) {
			return BehaviorMode.TASK_EXECUTION.logic;
		} else if(controller.canMine()) {
			return BehaviorMode.WANDER_AND_MINE.logic;
		} else if((controller.getLocation().distanceSquaredTo(controller.getRallyPoint()) > 200 && controller.getAlliesInRange().length < 10) || controller.getAlliesInRange().length < 3) {
			return BehaviorMode.REGROUP.logic;
		} else {
			return BehaviorMode.WANDER.logic;
		}
	}
	
	private static enum BehaviorMode {
		STOORM(new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				if(battleLost(controller)) {
					if(((canDoStuff(controller) && !retreat(controller, toLocation(controller, controller.getRallyPoint()))) 
							|| !cdBiggerThanRemainingCoreDelay(controller)) && canShoot(controller)) {
						 pewPewPew(controller, false);
					}
				} else {
					if(canShoot(controller)) {
						pewPewPew(controller, false);
					}
					if(canDoStuff(controller) && !ldBiggerThanRemainingWeaponDelay(controller)) {
						tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), false), false);
					}
				}
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				if(canDoStuff(controller))
					tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), false), battleLost(controller) || !(battleWon(controller) || alliesEngaged(controller)));
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				if(canDoStuff(controller))
					tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), false), false);
			}
		}), REGROUP(new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				if(!battleWon(controller)) {
					if(canDoStuff(controller)) {
						retreat(controller, toLocation(controller, controller.getRallyPoint()));
					}
					if(canShoot(controller) && !cdBiggerThanRemainingCoreDelay(controller)) {
						 pewPewPew(controller, false);
					}
				} else {
					if(canShoot(controller)) {
						pewPewPew(controller, false);
					}
					if(canDoStuff(controller) && !ldBiggerThanRemainingWeaponDelay(controller)) {
						tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), false), false);
					}
				}
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				if(canDoStuff(controller))
					tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), true), !(battleWon(controller)));
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				if(canDoStuff(controller))
					tryToMove(controller, controller.getNavigation().navigate(controller.getRallyPoint(), true), false);
			}
			
		}), SIEGE(new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				if(battleLost(controller)) {
					if(((canDoStuff(controller) && !retreat(controller, toLocation(controller, controller.getRallyPoint()))) 
							|| !cdBiggerThanRemainingCoreDelay(controller)) && canShoot(controller)) {
						 pewPewPew(controller, false);
					}
				} else {
					if(canShoot(controller)) {
						pewPewPew(controller, false);
					}
				}
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				if(!battleLost(controller)) {
					if(canDoStuff(controller) && alliesEngaged(controller)) {
						engage(controller);
					}
				}
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				if(canDoStuff(controller)&& controller.canMine()) {
					try {
						controller.mine();
					} catch (GameActionException e) {
						e.printStackTrace();
					}
				}
			}
		}), TASK_EXECUTION(new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				if(nearDeath(controller)) {
					secureSupplies(controller);
				}
				if(!battleWon(controller)) {
					if(((canDoStuff(controller) && !retreat(controller, toLocation(controller, controller.getRallyPoint()))) 
							|| !cdBiggerThanRemainingCoreDelay(controller)) && canShoot(controller)) {
						 pewPewPew(controller, false);
					}
				} else {
					if(canShoot(controller)) {
						pewPewPew(controller, false);
					}
				}
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				if(canDoStuff(controller)) {
					if(battleLost(controller)) {
						retreat(controller, toLocation(controller, controller.senseHQLocation()));
					} else if(battleWon(controller) && alliesEngaged(controller)) {
						engage(controller);
					} else if(controller.getTaskSystem().taskAssigned()) {
						if(!controller.getTaskSystem().getCurrentTask().perform(controller)) {
							if(controller.canMine()) {
								try {
									controller.mine();
								} catch (GameActionException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				if(controller.getTaskSystem().taskAssigned()) {
					if(!controller.getTaskSystem().getCurrentTask().perform(controller)) {
						if(canDoStuff(controller) && controller.canMine()) {
							try {
								controller.mine();
							} catch (GameActionException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}),
		WANDER_AND_MINE(new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				WANDER.logic.enemiesInRange(controller);
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				// TODO Maybe we just should ignore and still mine?
				WANDER.logic.enemiesNearby(controller);
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				double oreLocally = controller.senseOre(controller.getLocation());
				if(canDoStuff(controller)) {
					for(MapLocation location : MapLocation.getAllMapLocationsWithinRadiusSq(controller.getLocation(), 9)) {
						if(controller.senseOre(location) > 3*oreLocally || (toLocation(controller, location) == controller.getLastDirectionMoved() && controller.senseOre(location) > 2*oreLocally)) {
							try {
								if(!controller.isLocationOccupied(location) && controller.canMove(toLocation(controller, location))) {
									controller.move(toLocation(controller, location));
									break;
								}
							} catch (GameActionException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					if(canDoStuff(controller)) {
						try {
							controller.mine();
						} catch (GameActionException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}), WANDER(new LogicModule() {

			@Override
			public void enemiesInRange(AdvancedController controller) {
				REGROUP.logic.enemiesInRange(controller);
			}

			@Override
			public void enemiesNearby(AdvancedController controller) {
				REGROUP.logic.enemiesNearby(controller);
			}

			@Override
			public void noEnemies(AdvancedController controller) {
				if(canDoStuff(controller)) {
					Direction randDirection = Direction.values()[controller.getRandom().nextInt(8)];
					if(controller.getLocation().add(randDirection).distanceSquaredTo(controller.getRallyPoint()) <= MAX_WANDER_DISTANCE) {
						tryToMove(controller, randDirection, false);
					} else {
						tryToMove(controller, toLocation(controller, controller.senseHQLocation()), false);
					}
				}
			}
			
		});
		
		BehaviorMode(LogicModule logic) {
			this.logic = logic;
		}
		
		private LogicModule logic;
	}
}
