package george.core.logic;

import george.AdvancedController;

public interface LogicModule {
	
	public void enemiesInRange(AdvancedController controller);
	
	public void enemiesNearby(AdvancedController controller);
	
	public void noEnemies(AdvancedController controller);
}