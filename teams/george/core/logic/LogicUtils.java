package george.core.logic;

import george.AdvancedController;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class LogicUtils {
	private static Direction[] directions = {Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST, Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST};
	private static int[] dirOffsets = {0,1,-1,2,-2,3,-3,4};
	
	private LogicUtils() {}
	
	public static void shareSupplies(AdvancedController controller) {
		for(RobotInfo robot : controller.senseNearbyRobots(15, controller.getTeam())) {
			double supplyExcess = controller.getSupplyLevel()-750;
			if(supplyExcess <= 0) {
				break;
			}
			if(controller.getSupplyLevel()-robot.supplyLevel >= 100.0) {
				try {
					controller.transferSupplies((int)Math.min(supplyExcess,  (controller.getSupplyLevel()-robot.supplyLevel)/2), robot.location);
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void supplyNearbyUnits(AdvancedController controller) {
		for(RobotInfo robot : controller.senseNearbyRobots(15, controller.getTeam())) {
			if(controller.getSupplyLevel() < 1) {
				break;
			}
			if(!robot.type.isBuilding && robot.supplyLevel < 3000) {
				try {
					controller.transferSupplies((int)Math.min(controller.getSupplyLevel(),  3000-robot.supplyLevel), robot.location);
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static boolean nearDeath(AdvancedController controller) {
		return controller.getHealth() < controller.getType().maxHealth/5.0;
	}
	
	public static boolean canShoot(AdvancedController controller) {
		return controller.getType().canAttack() && controller.isWeaponReady();
	}
	
	public static boolean canDoStuff(AdvancedController controller) {
		return controller.isCoreReady();
	}
	
	public static boolean pewPewPew(AdvancedController controller, boolean splashDamage) {
		if(controller.getType() == RobotType.BASHER) {
			return controller.getEnemiesInRange().length > 0;
		}
		if(splashDamage) {
			// TODO Handle splash damage
			return pewPewPew(controller, false);
		} else {
			RobotInfo bestTarget = null;
			int bestValue = Integer.MIN_VALUE;
			for(RobotInfo robot : controller.getEnemiesInRange()) {
				int value = heuresticCombatValue(controller, robot);
				if(value > bestValue) {
					bestTarget = robot;
					bestValue = value;
				}
			}
			if(bestTarget != null) {
				try {
					controller.attackLocation(bestTarget.location);
					return true;
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	
	public static boolean isSafe(AdvancedController controller, MapLocation location) {
		for(RobotInfo robot : controller.getEnemiesNearby()) {
			if(robot.type == RobotType.MISSILE) {
				if(robot.location.distanceSquaredTo(location) <= 8) {
					return false;
				}
			} else {
				if(robot.type.canAttack() && robot.type.attackRadiusSquared >= robot.location.distanceSquaredTo(location)) {
					return false;
				}
			}
		}
		int enemyTowers = controller.senseEnemyTowerLocations().length;
		if(enemyTowers >= 5) {
			// Can we get hit by HQ splash damage?
			location = location.add(toEnemyHQ(controller));
			return controller.senseEnemyHQLocation().distanceSquaredTo(location) > 35;
		} else if(enemyTowers >= 2) {
			return controller.senseEnemyHQLocation().distanceSquaredTo(location) > 35;
		} else {
			return true;
		}
	}
	
	public static Direction toLocation(AdvancedController controller, MapLocation location) {
		return controller.getLocation().directionTo(location);
	}
	
	public static Direction toEnemyHQ(AdvancedController controller) {
		return toLocation(controller, controller.senseEnemyHQLocation());
	}
	
	public static RobotInfo closestRobot(AdvancedController controller, RobotInfo[] robots) {
		MapLocation location = controller.getLocation();
		RobotInfo closestRobot = null;
		int closestDistance = Integer.MAX_VALUE;
		for(RobotInfo robot : robots) {
			int distance = location.distanceSquaredTo(robot.location);
			if (distance < closestDistance) {
				closestRobot = robot;
				closestDistance = distance;
			}
		}
		return closestRobot;
	}
	
	/*
	 *  Logic for casual units
	 */
	
	public static boolean battleLost(AdvancedController controller) {
		double ourHealth = 0.0;
		double ourDPR = 0.0;
		double theirHealth = 0.0;
		double theirDPR = 0.0;
		for(RobotInfo robot : controller.getEnemiesNearby()) {
			theirDPR += (double)robot.type.attackPower/robot.type.attackDelay;
			theirHealth += robot.health;
		}
		for(RobotInfo robot : controller.getAlliesNearby()) {
			ourDPR += (double)robot.type.attackPower/robot.type.attackDelay;
			ourHealth += robot.health;
		}
		return (ourHealth / theirDPR) <= 0.7*(theirHealth / ourDPR);
	}
	
	public static void engage(AdvancedController controller) {
		// TODO
	}
	
	public static boolean battleWon(AdvancedController controller) {
		double ourHealth = 0.0;
		double ourDPR = 0.0;
		double theirHealth = 0.0;
		double theirDPR = 0.0;
		for(RobotInfo robot : controller.getEnemiesNearby()) {
			theirDPR += (double)robot.type.attackPower/robot.type.attackDelay;
			theirHealth += robot.health;
		}
		for(RobotInfo robot : controller.getAlliesNearby()) {
			ourDPR += (double)robot.type.attackPower/robot.type.attackDelay;
			ourHealth += robot.health;
		}
		return (theirHealth / ourDPR) <= 0.7*(ourHealth / theirDPR);
	}
	
	public static boolean alliesEngaged(AdvancedController controller) {
		// TODO
		return false;
	}
	
	public static boolean ldBiggerThanRemainingWeaponDelay(AdvancedController controller) {
		return controller.getType().loadingDelay > controller.getWeaponDelay();
	}

	public static boolean cdBiggerThanRemainingCoreDelay(AdvancedController controller) {
		return controller.getType().cooldownDelay > controller.getCoreDelay();
	}

	public static boolean secureSupplies(AdvancedController controller) {
		// TODO Transfer supplies to nearby supplier or, if none close, to the most powerful and least supplied ally
		return false;
	}
	
	public static boolean tryToMove(AdvancedController controller, Direction direction, boolean avoidEnemies) {
		for(int offsetIndex = 0; offsetIndex < 4; offsetIndex++) {
			Direction offsetDirection;
			if(controller.getRandom().nextBoolean()) {
				offsetDirection = turnClockwise(direction, dirOffsets[offsetIndex]);
			} else {
				offsetDirection = turnCounterClockwise(direction, dirOffsets[offsetIndex]);
			}
			
			if(controller.canMove(offsetDirection) && (!avoidEnemies || isSafe(controller, controller.getLocation().add(offsetDirection)))) {
				try {
					controller.move(offsetDirection);
					return true;
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	public static boolean retreat(AdvancedController controller, Direction preferredDirection) {
		// TODO
		return false;
	}
	
	/*
	 * Logic for beavers only
	 */
	
	public static boolean tryToBuild(AdvancedController controller, RobotType type, Direction preferable) {
		for(int offsetIndex = 0; offsetIndex < 8; offsetIndex++) {
			Direction offsetDirection;
			if(controller.getRandom().nextBoolean()) {
				offsetDirection = turnClockwise(preferable, dirOffsets[offsetIndex]);
			} else {
				offsetDirection = turnCounterClockwise(preferable, dirOffsets[offsetIndex]);
			}
			
			if(controller.canBuild(offsetDirection, type)) {
				try {
					controller.build(offsetDirection, type);;
					return true;
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
		return false;	
	}
	
	/*
	 * Logic for buildings
	 */
	
	public static boolean tryToSpawn(AdvancedController controller, RobotType type, Direction preferable) {
		for(int offsetIndex = 0; offsetIndex < 8; offsetIndex++) {
			Direction offsetDirection;
			if(controller.getRandom().nextBoolean()) {
				offsetDirection = turnClockwise(preferable, dirOffsets[offsetIndex]);
			} else {
				offsetDirection = turnCounterClockwise(preferable, dirOffsets[offsetIndex]);
			}
			
			if(controller.canSpawn(offsetDirection, type)) {
				try {
					controller.spawn(offsetDirection, type);;
					return true;
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	
	/*
	 * Additional functions
	 */
	
	// TODO Set the values up so that the final value will always be > 0
		public static int heuresticCombatValue(AdvancedController controller, RobotInfo robot) {
			// TODO Consider missles separately, we have to focus them early so that they won't harm us
			
			int value = controller.getConfig().HEURESTIC_INITIAL_VALUE;
			value += (robot.type.attackPower*controller.getConfig().HEURESTIC_DPR_MULTIPLIER)/robot.type.attackDelay;
			value -= robot.health*controller.getConfig().HEURESTIC_HEALTH_MULTIPLIER;
			value += robot.supplyLevel*controller.getConfig().HEURESTIC_SUPPLY_MULTIPLIER;
			if(robot.type == RobotType.HQ) {
				value += controller.getConfig().HEURESTIC_HQ_BONUS;
			} else if(robot.type == RobotType.TOWER) {
				value += controller.getConfig().HEURESTIC_TOWER_BONUS;
			} else if(robot.type.isBuilding) {
				value += controller.getConfig().HEURESTIC_FUNCTIONAL_BONUS;
			}
			return value;
		}
	
	public static Direction turnClockwise(Direction direction, int times) {
		return directions[(8+(directionToInt(direction)+times))%8];
	}
	
	public static Direction turnCounterClockwise(Direction direction, int times) {
		return directions[(8+(directionToInt(direction)-times))%8];
	}
	
	public static int directionToInt(Direction direction) {
		switch(direction) {
			case NORTH:
				return 0;
			case NORTH_EAST:
				return 1;
			case EAST:
				return 2;
			case SOUTH_EAST:
				return 3;
			case SOUTH:
				return 4;
			case SOUTH_WEST:
				return 5;
			case WEST:
				return 6;
			case NORTH_WEST:
				return 7;
			default:
				return -1;
		}
	}
}
