package george.macro;

import george.AdvancedController;
import george.map.MapUtils;
import george.messaging.MessagingSystem.ChannelType;
import george.messaging.MessagingSystem.InvalidChannelIndexException;
import george.task.Task.TaskType;
import george.task.TaskSystem.TaskListener;
import battlecode.common.Clock;
import battlecode.common.MapLocation;
import battlecode.common.RobotType;

public class Economy {
	private AdvancedController controller;
	
	private final int ROUNDS_CONSIDERED = 3;
	
	public Economy(AdvancedController controller) {
		this.controller = controller;
	}
	
	public void centralizedUpdate() {
		controller.setIndicatorString(0, String.valueOf(-4/3));
		if(Clock.getRoundNum() % 25 == 10) {
			controller.getTaskSystem().scheduleTask(TaskType.COMPUTE_ROBOT_COUNTS);
		}
		if(Clock.getRoundNum() % 250 == 125) {
			planConstructions();
		}
	}
	
	public void update() {
		try {
			controller.getMessagingSystem().broadcast((int)getAvailableOre(), ChannelType.ORE_PROFILING, Clock.getRoundNum());
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
		}
	}
	
	// Note: This can be positive or negative
	public double getRecentOreGainRatio() {
		try {
			return getAvailableOre()-controller.getMessagingSystem().read(ChannelType.ORE_PROFILING, Math.max(0, Clock.getRoundNum()-ROUNDS_CONSIDERED));
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
			return 0.0;
		}
	}
	
	public boolean isDesirable(RobotType type) {
		if(!canAfford(type)) {
			return false;
		}
		return getRobotCount(type) <= controller.getStrategyProfile().getCurrentStrategy().getRobotCountLimit(type);
	}
	
	public boolean canAfford(RobotType type) {
		return type.oreCost <= getAvailableOre();
	}
	
	private void planConstructions() {
		// TODO All the code below have to be replaced with something serious
		if(Clock.getRoundNum() > 125) {
			scheduleConstruction(RobotType.HELIPAD, MapUtils.moveLocation(controller.senseHQLocation(), 2, 2));
			scheduleConstruction(RobotType.HELIPAD, MapUtils.moveLocation(controller.senseHQLocation(), 4, -1));
			scheduleConstruction(RobotType.HELIPAD, MapUtils.moveLocation(controller.senseHQLocation(), 12, 0));
		} else {
			scheduleConstruction(RobotType.HANDWASHSTATION, MapUtils.moveLocation(controller.senseHQLocation(), -7, 1));
		}
	}
	
	public void scheduleConstruction(final RobotType type, MapLocation destination) {
		reserveOre(type.oreCost);
		
		controller.getTaskSystem().scheduleTask(TaskType.BUILD, (byte)type.ordinal(), destination, new TaskListener() {

			@Override
			public void onSuccess() {
				releaseOre(type.oreCost);
			}

			@Override
			public void onFailure() {
				releaseOre(type.oreCost);
			}
		});
	}
	
	public double getAvailableOre() {
		try {
			return controller.getTeamOre()-controller.getMessagingSystem().read(ChannelType.RESERVED_ORE, 0);
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
			return 0.0;
		}
	}
	
	public void reserveOre(int amount) {
		// Maybe we should rather throw an exception here
		if(amount < 0) {
			return;
		}
		try {
			int reservedOre = controller.getMessagingSystem().read(ChannelType.RESERVED_ORE, 0);
			reservedOre += amount;
			controller.getMessagingSystem().broadcast(reservedOre, ChannelType.RESERVED_ORE, 0);
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
		}
	}
	
	public void releaseOre(int amount) {
		// Maybe we should rather throw an exception here
		if(amount < 0) {
			return;
		}
		try {
			int reservedOre = controller.getMessagingSystem().read(ChannelType.RESERVED_ORE, 0);
			reservedOre = Math.max(0, reservedOre-amount);
			controller.getMessagingSystem().broadcast(reservedOre, ChannelType.RESERVED_ORE, 0);
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
		}
	}
	
	public int getRobotCount(RobotType type) {
		try {
			return controller.getMessagingSystem().read(ChannelType.ROBOT_COUNTS, type.ordinal());
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
			return 0;
		}
	}
}
