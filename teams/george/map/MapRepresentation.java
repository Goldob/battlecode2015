package george.map;

import george.AdvancedController;
import george.messaging.MessagingSystem.ChannelType;
import george.messaging.MessagingSystem.InvalidChannelIndexException;
import battlecode.common.Direction;
import battlecode.common.MapLocation;
import battlecode.common.TerrainTile;

// TODO Try playing with map symmetry (maybe it should be a part of AdvancedController sensing
public class MapRepresentation {
	private AdvancedController controller;
	
	public MapRepresentation(AdvancedController controller) {
		this.controller = controller;
	}
	
	public void recompute() {
		for(byte x = -43; x < 43; x++) {
			for(byte y = -43; y < 43; y++) {
				tryToComputeNode(getNode(x, y));
			}
		}
	}
	
	public CoarsenedNode getNodeAtLocation(MapLocation location) {
		byte[] coords = MapUtils.locToRelativeCoords(controller, location);
		int x = coords[0]/3;
		int y = coords[1]/3;
		
		if(coords[0] < 0)
			x--;
		if(coords[1] < 0)
			y--;
		return getNode((byte)x,(byte)y);
	}
	
	public int getIndex(CoarsenedNode node) {
		return getIndex(node.x, node.y);
	}
	
	private int getIndex(int x, int y) {
		return (x+43)*86+(y+43);
	}
	
	public CoarsenedNode getNodeFromIndex(int index) {
		byte y = (byte) ((index % 86)-43);
		byte x = (byte) (((index - y)/86) - 43);
		return getNode(x, y);
	}
	
	private CoarsenedNode getNode(byte x, byte y) {
		if(x < -43 || x > 43 || y < -43 || y > 43) {
			return new CoarsenedNode(x, y, 0);
		}
		try {
			return new CoarsenedNode(x, y, controller.getMessagingSystem().read(ChannelType.MAP_REPRESENTATION, getIndex(x, y)));
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private void saveNode(CoarsenedNode node) {
		if(node.x < -43 || node.x > 43 || node.y < -43 || node.y > 43) {
			return;
		}
		try {
			controller.getMessagingSystem().broadcast(node.getRawData(), ChannelType.MAP_REPRESENTATION, getIndex(node.x, node.y));
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
		}
	}
	
	private void tryToComputeNode(CoarsenedNode node) {
		MapLocation centralLocation = getCentralLocation(node);
		if(controller.senseTerrainTile(centralLocation) == TerrainTile.UNKNOWN) {
			return;
		}
		if(controller.senseTerrainTile(centralLocation) == TerrainTile.NORMAL) {
			node.setTraversable(true);
			for(int i = 0; i < 8; i++) {
				Direction dir = Direction.values()[i];
				MapLocation adjacentLocation = centralLocation.add(dir);
				if(controller.senseTerrainTile(adjacentLocation) == TerrainTile.UNKNOWN) {
					return;
				}
				
				CoarsenedNode neighbor = getNeighbor(node, dir);
				if(neighbor.computed && neighbor.traversable) {
					if(controller.senseTerrainTile(adjacentLocation) == TerrainTile.NORMAL && controller.senseTerrainTile(adjacentLocation.add(dir)) == TerrainTile.NORMAL) {
						node.createConnection(dir);
						neighbor.createConnection(dir.opposite());
						saveNode(neighbor);
					}
				}
			}
		} else {
			node.setTraversable(false);
		}
		saveNode(node);
	}
	
	public CoarsenedNode getNeighbor(CoarsenedNode node, Direction direction) {
		return getNode((byte)(node.x+direction.dx), (byte)(node.y+direction.dy));
	}
	
	public MapLocation getCentralLocation(CoarsenedNode node) {
		return MapUtils.locFromRelativeCoords(controller, (byte)(3*node.x), (byte)(3*node.y));
	}
	
	public static class CoarsenedNode {
		private boolean[] connected = new boolean[8];
		private byte x;
		private byte y;
		
		boolean computed;
		boolean traversable = false;
		
		protected CoarsenedNode(byte x, byte y, int nodeData) {
			this.x = x;
			this.y = y;
			
			for (int i = 8; --i >= 0;) {
				connected[i] = (nodeData & (1 << i)) != 0;
			}
			
			computed = (nodeData & (1 << 31)) != 0;
			traversable = (nodeData & 1 << 30) != 0;
		}
		
		public void setTraversable(boolean traversable) {
			this.traversable = traversable;
		}

		protected void createConnection(Direction direction) {
			connected[direction.ordinal()] = true;
		}
		
		protected int getRawData() {
			int data = 0;
			for (int i = 8; --i >= 0;) {
				if (connected[i]) {
					data = data | (1 << i);
				}
			}
			
			return (1 << 31) | ((traversable ? 1 : 0) << 30) | data;
		}
		
		public byte getRelativeX() {
			return x;
		}
		
		public byte getRelativeY() {
			return y;
		}
		
		public boolean isTraversable() {
			return traversable;
		}
		
		public boolean isConnected(Direction direction) {
			int ordinal = direction.ordinal();
			if (ordinal <= 7) {
				return connected[ordinal];
			}
			return false;
		}
		
		Direction a;
	}
}
