package george.map;

import george.AdvancedController;
import battlecode.common.MapLocation;

public class MapUtils {
	public static MapLocation locFromRelativeCoords(AdvancedController controller, byte coordX, byte coordY) {
		return moveLocation(controller.senseHQLocation(), coordX, coordY);
	}
	
	public static byte[] locToRelativeCoords(AdvancedController controller, MapLocation location) {
		return new byte[] {
				(byte) (location.x-controller.senseHQLocation().x),
				(byte) (location.y-controller.senseHQLocation().y)
		};
	}
	
	public static MapLocation moveLocation(MapLocation location, int coordX, int coordY) {
		return new MapLocation(location.x+coordX, location.y+coordY);
	}
}
