package george.navigation;

import george.AdvancedController;
import george.core.logic.LogicUtils;
import battlecode.common.Direction;
import battlecode.common.MapLocation;
import battlecode.common.RobotType;

public class Navigation {
	private AdvancedController controller;
	private NavigationMode navMode = NavigationMode.NOT_INITIALIZED;
	
	public Navigation(AdvancedController controller) {
		this.controller = controller;
		
		// TODO Change navigation mode depending on the unit type
		if(controller.getType() == RobotType.DRONE) {
			navMode = NavigationMode.NOCLIP;
		} else {
			navMode = NavigationMode.BUG_NAV;
		}
		navMode.init(controller);
	}
	
	public Direction navigate(MapLocation destination, boolean avoidEnemyHQ) {
		return navMode.navigate(controller, destination, avoidEnemyHQ);
	}
	
	public void setMode(NavigationMode navMode) {
		this.navMode = navMode;
		navMode.init(controller);
	}
	
	public static enum NavigationMode {
		NOT_INITIALIZED {
			@Override
			public void init(AdvancedController controller) {}

			@Override
			public Direction navigate(AdvancedController controller,
					MapLocation destination, boolean avoidEnemyHQ) {
				return null;
			}
		},
		BUG_NAV {
			private boolean turnRight;
			
			@Override
			public void init(AdvancedController controller) {
				turnRight = controller.getRandom().nextBoolean();
			}

			@Override
			public Direction navigate(AdvancedController controller, MapLocation destination, boolean avoidEnemyHQ) {
				Direction direction = LogicUtils.toLocation(controller, destination);
				for(int count = 0; count < 8; count++) {
					if(controller.canMove(direction)) {
						break;
					}
					direction = (turnRight ? direction.rotateRight() : direction.rotateLeft());
				}
				return direction;
			}
		},
		NOCLIP {
			@Override
			public void init(AdvancedController controller) {}

			@Override
			public Direction navigate(AdvancedController controller, MapLocation destination, boolean avoidEnemyHQ) {
				// TODO Maybe some more advanced stuff when we are near enemy HQ
				return LogicUtils.toLocation(controller, destination);
			}
		},
		BFS {
			private BFSPathing bfs;
			
			@Override
			public void init(AdvancedController controller) {
				bfs = new BFSPathing(controller);
			}

			@Override
			public Direction navigate(AdvancedController controller,
					MapLocation destination, boolean avoidEnemyHQ) {
				return bfs.getNextStep(destination);
			}
		};
		
		public abstract void init(AdvancedController controller);
		
		public abstract Direction navigate(AdvancedController controller, MapLocation destination, boolean avoidEnemyHQ);
	}
}
