package george.task;

import george.AdvancedController;
import george.core.logic.LogicUtils;
import george.map.MapUtils;
import george.messaging.MessagingSystem;
import george.messaging.MessagingSystem.ChannelType;
import george.messaging.MessagingSystem.InvalidChannelIndexException;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

//TODO
public class Task {
	private int taskID;
	private TaskType taskType;
	private byte data;
	private MapLocation targetLocation;
	
	private TaskStatus status;
	private int claimerID;
	
	public Task(int taskID, TaskType type, byte data, MapLocation targetLocation) {
		this.taskID = taskID;
		this.taskType = type;
		this.data = data;
		this.targetLocation =  targetLocation;
		
		status = TaskStatus.NO_IDEA;
	}
	
	protected void updateStatus(AdvancedController controller) {
		try {
			int rawStatus = controller.getMessagingSystem().read(ChannelType.TASK_STATUS, taskID);
			status = TaskStatus.values()[(rawStatus >> 24) & 0xff];

			claimerID = rawStatus & 0x00ffffff;
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
		}
	}
	
	// Returns false if IFF no actions were performed
	public boolean perform(AdvancedController controller) {
		return taskType.perform(controller, this);
	}
	
	protected MapLocation getTargetLocation() {
		return targetLocation;
	}
	
	public void reportFalure(AdvancedController controller) {
		changeStatus(controller, TaskStatus.FAILED, false);
	}
	
	public void reportSuccess(AdvancedController controller) {
		changeStatus(controller, TaskStatus.SUCEEDED, false);
	}

	public void changeStatus(AdvancedController controller, TaskStatus status, boolean claim) {
		this.status = status;
		if(claim) {
			claimerID = controller.getID();
		}
		try {
			controller.getMessagingSystem().broadcast(getRawStatus(), ChannelType.TASK_STATUS, taskID);
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
		}
	}
	
	public int getClaimerID() {
		return claimerID;
	}
	
	public int getID() {
		return taskID;
	}
	
	public TaskType getType() {
		return taskType;
	}
	
	public int getRawStatus() {
		return ((status.ordinal() << 24) & 0xff000000) | (claimerID & 0x00ffffff);
	}
	
	public int getRawDefinition(AdvancedController controller) {
		byte[] relativeCoords = MapUtils.locToRelativeCoords(controller, targetLocation);
		return MessagingSystem.fromBytes((byte)taskType.ordinal(), data, relativeCoords[0], relativeCoords[1]);
	}
	
	private int getData() {
		return data;
	}
	
	public TaskStatus getStatus() {
		return status;
	}
	
	public static enum TaskType {
		NONE(false) {
			@Override
			public boolean perform(AdvancedController controller, Task task) {
				return false;
			}
		}, COMPUTE_ROBOT_COUNTS(true) {
			@Override
			public boolean perform(AdvancedController controller, Task task) {
				int[] counts = new int[20];
				int count = 0;
				for(RobotInfo robot : controller.senseNearbyRobots(14400, controller.getTeam())) {
					counts[robot.type.ordinal()]++;
					count++;
				}
				try {
					for(int i = 0; i < 20; i++) {
						try {
							controller.getMessagingSystem().broadcast(counts[i], ChannelType.ROBOT_COUNTS, i);
						} catch (InvalidChannelIndexException e) {
							e.printStackTrace();
						}
					}
					
					controller.getMessagingSystem().broadcast(count, ChannelType.ROBOT_COUNT, 0);
				} catch (InvalidChannelIndexException e) {
					e.printStackTrace();
				}
				task.reportSuccess(controller);
				return false;
			}
		}, COMPUTE_MAP_REPRESENTATION(true) {
			@Override
			public boolean perform(AdvancedController controller, Task task) {
				controller.getMapRepresentation().recompute();
				task.reportSuccess(controller);
				return false;
			}
		}, SUPPLY(false) {
			
			// TODO Run in circles at the point
			
			@Override
			public boolean perform(AdvancedController controller, Task task) {
				MapLocation destination;
				if(task.getData() == (byte)0x00) {
					destination = controller.senseHQLocation();
				} else {
					destination = task.getTargetLocation();
				}
				if(controller.getLocation().distanceSquaredTo(destination) > 15) {
					LogicUtils.tryToMove(controller, LogicUtils.toLocation(controller, destination), true);
				} else if(task.getData() != 0x00) {
					LogicUtils.supplyNearbyUnits(controller);
				}
				return false;
			}
		}, SCOUT(false) {
			@Override
			public boolean perform(AdvancedController controller, Task task) {
				if(controller.canSenseLocation(task.getTargetLocation())) {
					task.reportSuccess(controller);
					return false;
				}
				if(LogicUtils.canDoStuff(controller)) {
					LogicUtils.tryToMove(controller, LogicUtils.toLocation(controller, task.getTargetLocation()), true);
				}
				return false;
			}
		}, BUILD(false) {
			@Override
			public boolean perform(AdvancedController controller, Task task) {
				controller.setIndicatorString(2, task.getTargetLocation().toString());
				if(LogicUtils.canDoStuff(controller)) {
					if(controller.getLocation().distanceSquaredTo(task.getTargetLocation()) <= 2) {
						Direction direction = LogicUtils.toLocation(controller, task.getTargetLocation());
						// When the case is that we are standing on the target location
						if(direction == Direction.NONE || direction == Direction.OMNI) {
							direction = LogicUtils.toEnemyHQ(controller).opposite();
						}
						if(LogicUtils.tryToBuild(controller, RobotType.values()[task.getData()], direction)) {
							task.reportSuccess(controller);
							return true;
						}
					} else {
						return LogicUtils.tryToMove(controller, controller.getNavigation().navigate(task.getTargetLocation(), true), true);
					}
				}
				return false;
			}
		}, WASH_HANDS(false) {

			@Override
			public boolean perform(AdvancedController controller, Task task) {
				if(controller.isClean()) {
					task.reportSuccess(controller);
					return false;
				}
				if(LogicUtils.canDoStuff(controller)) {
					if(controller.getLocation().distanceSquaredTo(task.getTargetLocation()) <= 2) {
						try {
							controller.washHands(task.getTargetLocation());
							return true;
						} catch (GameActionException e) {
							e.printStackTrace();
							task.reportFalure(controller);
							return false;
						}
					} else {
						return LogicUtils.tryToMove(controller, controller.getNavigation().navigate(task.getTargetLocation(), true), true);
					}
				}
				return false;
			}
			
		};
		
		private TaskType(boolean computable) {
			this.computable = computable;
		}
		private boolean computable;
		
		public abstract boolean perform(AdvancedController controller, Task task);
		
		public boolean isComputable() {
			return computable;
		}
	}
	
	public static enum TaskStatus {
		NO_IDEA, SCHEDULED, IN_PROGRESS, SUCEEDED, FAILED;
	}
}
