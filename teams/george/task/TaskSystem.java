package george.task;

import george.AdvancedController;
import george.map.MapUtils;
import george.messaging.MessagingSystem;
import george.messaging.MessagingSystem.ChannelType;
import george.messaging.MessagingSystem.InvalidChannelIndexException;
import george.task.Task.TaskStatus;
import george.task.Task.TaskType;

import java.util.LinkedList;
import java.util.List;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class TaskSystem {
	private final int TASK_SEARCH_RANGE = 20;
	
	protected AdvancedController controller;
	
	Task currentTask = null;
	boolean taskClaimed;
	boolean taskAssigned;
	
	private List<TaskTracker> taskTrackers;
	
	public TaskSystem(AdvancedController controller) {
		this.controller = controller;
		taskTrackers = new LinkedList<TaskTracker>();
	}
	
	public void update(boolean lookForTask) {
		// TODO Maybe do it every X rounds?
		controller.setIndicatorString(0, String.valueOf(taskTrackers.size()));
		List<TaskTracker> tempList = new LinkedList<TaskTracker>();
		for(TaskTracker tracker : taskTrackers) {
			tracker.getTask().updateStatus(controller);
			if(tracker.getTask().getStatus() == TaskStatus.SUCEEDED) {
				tracker.getListener().onSuccess();
			} else if(tracker.getTask().getStatus() == TaskStatus.FAILED) {
				tracker.getListener().onFailure();
			} else {
				tempList.add(tracker);
			}
		}
		
		taskTrackers = tempList;
		
		if(currentTask != null) {
			currentTask.updateStatus(controller);
			if(taskAssigned && currentTask.getStatus() != TaskStatus.IN_PROGRESS) {
				currentTask = null;
				taskClaimed = false;
				taskAssigned = false;
			}
			if(taskClaimed && !taskAssigned) {
				if(currentTask.getClaimerID() == controller.getID() && currentTask.getStatus() == TaskStatus.SCHEDULED) {
					currentTask.changeStatus(controller, TaskStatus.IN_PROGRESS, false);
					taskAssigned = true;
				}
			}
		}
		if(lookForTask && !taskClaimed) {
			claimMostSuitableUnassignedTask();
		}
	}
	
	public void scanForDeadClaimersAndFailTasks() {
		try {
			for(int taskID = controller.getMessagingSystem().read(ChannelType.NEXT_TASK_ID, 0)-1; taskID >= 0; taskID--) {
				Task task = taskFromRawDefinition(taskID, MessagingSystem.toByteArray(controller.getMessagingSystem().read(ChannelType.TASK_DEFINITION, taskID)));
				task.updateStatus(controller);
				if(!controller.canSenseRobot(task.getClaimerID())) {
					task.reportFalure(controller);
				}
			}
		} catch(InvalidChannelIndexException e) {
			e.printStackTrace();
		}
	}
	
	public boolean taskAssigned() {
		return taskAssigned;
	}
	
	public Task getCurrentTask() {
		return currentTask;
	}
	
	private void claimMostSuitableUnassignedTask() {
		try {
			Task chosenTask = null;
			
			int last_taskID = controller.getMessagingSystem().read(ChannelType.NEXT_TASK_ID, 0)-1;
			int first_taskID = Math.max(0, last_taskID-TASK_SEARCH_RANGE+1);
			for(int taskID = first_taskID; taskID <= last_taskID; taskID++) {
				byte[] taskDefinition = MessagingSystem.toByteArray(controller.getMessagingSystem()
						.read(ChannelType.TASK_DEFINITION, taskID));
				Task task = taskFromRawDefinition(taskID, taskDefinition);
				task.updateStatus(controller);
				if(task.getStatus() == TaskStatus.SCHEDULED) {
					if(isNotWorseForMe(task, chosenTask) && amIBetterForTheTask(task)) {
						chosenTask = task; 
					}
				}
			}
			if(chosenTask != null) {
				chosenTask.changeStatus(controller, TaskStatus.SCHEDULED, true);
				currentTask = chosenTask;
				taskClaimed = true;
			}
		} catch(InvalidChannelIndexException e) {
			e.printStackTrace();
		}
	}
	
	public Task scheduleTask(TaskType type, byte data, MapLocation destination) {
		Task task = null;
		try {
			int taskID = controller.getMessagingSystem().read(ChannelType.NEXT_TASK_ID, 0);
			task = new Task(taskID, type, data, destination);
			controller.getMessagingSystem().broadcast(task.getRawDefinition(controller), ChannelType.TASK_DEFINITION, taskID);
			task.changeStatus(controller, TaskStatus.SCHEDULED, false);
			controller.getMessagingSystem().broadcast(taskID+1, ChannelType.NEXT_TASK_ID, 0);
		} catch (InvalidChannelIndexException e) {
			e.printStackTrace();
		}
		return task;
	}
	
	public Task scheduleTask(TaskType type, byte data, MapLocation destination, TaskListener listener) {
		Task task = scheduleTask(type, data, destination);
		if(task != null) {
			taskTrackers.add(new TaskTracker(task, listener));
		}
		return task;
	}
	
	public Task scheduleTask(TaskType type) {
		return scheduleTask(type, (byte)0, new MapLocation(0,0));
	}
	
	public Task scheduleTask(TaskType type, TaskListener listener) {
		return scheduleTask(type, (byte)0, new MapLocation(0,0), listener);
	}
	
	private Task taskFromRawDefinition(int id, byte[] definition) {
		return new Task(id, TaskType.values()[definition[0]], definition[1], MapUtils.locFromRelativeCoords(controller, definition[2], definition[3]));
	}
	
	private boolean isNotWorseForMe(Task thisTask, Task thanThis) {
		RobotType type = controller.getType();
		if(thisTask.getType() == TaskType.WASH_HANDS && controller.isClean()) {
			return false;
		}
		if(type.isBuilding || type == RobotType.COMPUTER) {
			return thisTask.getType().isComputable();
		} else if(type == RobotType.BEAVER) {
			if(thanThis == null || thanThis.getType().isComputable()) {
				return true;
			} else {
				if(thisTask.getType().isComputable()) {
					return false;
				} else {
					int distanceSqDiff = controller.getLocation().distanceSquaredTo(thisTask.getTargetLocation()) - controller.getLocation().distanceSquaredTo(thanThis.getTargetLocation());
					if(distanceSqDiff < 0) {
						return true;
					} else if(distanceSqDiff > 0) {
						return false;
					} else {
						if(thisTask.getType() == TaskType.BUILD) {
							return true;
						} else {
							return thanThis.getType() != TaskType.BUILD;
						}
					}
				}
			}
		} else {
			if(thanThis == null || thanThis.getType().isComputable()) {
				return thisTask.getType() == TaskType.SCOUT || thisTask.getType().isComputable();
			} else if(thisTask.getType() == TaskType.SCOUT){
				return controller.getLocation().distanceSquaredTo(thisTask.getTargetLocation()) <= controller.getLocation().distanceSquaredTo(thanThis.getTargetLocation());
			} else {
				return false;
			}
		}
	}
	
	private boolean amIBetterForTheTask(Task task) {
		if(!controller.canSenseRobot(task.getClaimerID())) {
			return true;
		}
		RobotInfo claimer;
		try {
			claimer = controller.senseRobot(task.getClaimerID());
		} catch (GameActionException e) {
			e.printStackTrace();
			return true;
		}
		if(task.getType() == TaskType.BUILD) {
			return controller.getLocation().distanceSquaredTo(task.getTargetLocation()) < claimer.location.distanceSquaredTo(task.getTargetLocation());
		} else if(task.getType() == TaskType.SCOUT || task.getType() == TaskType.WASH_HANDS) {
			return (controller.getType().movementDelay^2)*controller.getLocation().distanceSquaredTo(task.getTargetLocation()) 
					< (claimer.type.movementDelay^2)*claimer.location.distanceSquaredTo(task.getTargetLocation());
		} else {
			// Only computable tasks left
			if(claimer.type == RobotType.HQ) {
				return true;
			} else if(claimer.type != RobotType.COMPUTER && controller.getType() == RobotType.COMPUTER) {
				return true;
			} else if(claimer.type == RobotType.COMPUTER && controller.getType() != RobotType.COMPUTER) {
				return false;
			} else if(claimer.type.isBuilding && !controller.getType().isBuilding) {
				return true;
			} else if(!claimer.type.isBuilding && controller.getType().isBuilding) {
				return false;
			} else {
				return controller.getLocation().distanceSquaredTo(controller.senseEnemyHQLocation()) < claimer.location.distanceSquaredTo(controller.senseEnemyHQLocation());
			}
		}
	}
	
	public static interface TaskListener {
		public void onSuccess();
		
		public void onFailure();
	}
	
	public static class TaskTracker {
		private Task task;
		private TaskListener listener;
		
		public TaskTracker(Task task, TaskListener listener) {
			this.task = task;
			this.listener = listener;
		}
		
		public Task getTask() {
			return task;
		}
		
		public TaskListener getListener() {
			return listener;
		}
	}
}