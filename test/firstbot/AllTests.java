package firstbot;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import firstbot.messaging.MessagingSystemTest;

@RunWith(Suite.class)
@SuiteClasses({ MessagingSystemTest.class })
public class AllTests {

}
