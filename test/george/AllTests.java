package george;

import george.messaging.MessagingSystemTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MessagingSystemTest.class })
public class AllTests {

}
