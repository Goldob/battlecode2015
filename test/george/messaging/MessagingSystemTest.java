package george.messaging;

import static org.junit.Assert.assertEquals;
import george.messaging.MessagingSystem;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MessagingSystemTest {

	@Test
	public void testFromBytes() {
		assertEquals(0xaf4bcca6, MessagingSystem.fromBytes((byte)0xaf, (byte)0x4b, (byte)0xcc, (byte)0xa6));
	}
}
