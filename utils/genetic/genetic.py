#The following script was written for Python 3.x and may not work properly
#with lower versions

from random import randint
from tempfile import mkstemp
from shutil import move
from os import remove, close
import os.path
import re
import sys
from subprocess import Popen, PIPE
import xml.etree.ElementTree as ET

class EvolvingEntity:
    #variables is a dictionary variable -> value
    def __init__(self, score=0, variables=[]):
        self.variables = variables
        self.score = score

class TestEnvironment:
    def __init__(self, name, bot, variables, constants, maps, enemies):
         self.name = name
         self.bot = bot
         self.variables = variables
         self.constants = constants
         self.maps = maps
         self.enemies = enemies
    def generateEntity():
        entity_variables = {}
        for variable in variables:
            entity_variables[variable] = randint(variable[0], variable[1])
        return EvolvingEntity(0, variables)
    def crossover(entity1, entity2):
        child_variables = {}
        for variable in variables:
            if randint(0, 1):
                child_variables[variable] = entity2.variables[variable]
            else:
                child_variables[variable] = entity1.variables[variable]
        return EvolvingEntity(child_variables)
    def mutate(entity):
        for variable in variables:
            if not randint(0, 100):
                entity.variables[variable] = randint(variable[0], variable[1])
    def produceChild(entity1, entity2):
        child = crossover(entity1, entity2)
        mutate(child)
        return child
                    
def loadEnvironment(name):
    xml_tree = ET.parse('definitions.xml')
    for xml_environment in xml_tree.getroot().findall('environment'):
        if xml_environment.attributes['name'] is not name:
            continue
        bot = xml_environment.attributes['bot']
        variables = {}
        for xml_variable in xml_environment.find('variable'):
            variables[xml_variable.attributes['name']] = (xml_variable.attributes['min'], variable.attributes['max'])
        constants = {}
        for xml_constant in environment.find('constant'):
            constants[xml_constant.attributes['name']] = xml_constant.attributes['value']
        maps = []
        for xml_map in environment.find('map'):
            maps.append(xml_map.attributes['name'])
        enemies = []
        for xml_enemy in environment.find('enemy'):
            enemies.append(xml_enemy.attributes['team'])
        return TestEnvironment(name, bot, variables, constants, maps, enemies)

#Borrowed from Battlecode 2014 schnitzel team
def replaceConf(pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    new_file = open(abs_path,'w')
    old_file = open('{}/bc.conf'.format(dirname(dirname(os.path.abspath(__file__)))))
    for line in old_file:
        new_file.write(re.sub(pattern,subst,line))
        #Close temp file
        new_file.close()
        close(fh)
        old_file.close()
        #Remove original file
        remove(file_path)
        #Move new file
        move(abs_path, file_path)

def initConf(environment):
    replaceConf(r'(?:bc\.game\.team-a=).*', environment.bot)
    for constant in environment.constants:
        replaceConf(r'(?:bc\.testing\.team-a\.{}=).*'.format(constant), environment.constants[constant])

def didAWin(output):
    #TODO
    return False

def calculateScore(entity, environment):
    score = 0
    for variable in environment.variables:
        replaceConf(r'(?:bc\.testing\.team-a\.{}=).*'.format(variable), environment.variables[variable])
    for enemy in environment.enemies:
        replaceConf(r'(?:bc\.game\.team-b=).*', enemy)
        for game_map in environment.maps:
            replaceConf(r'(?:bc\.game\.maps=).*', game_map)
            output = Popen('ant -find', stdout=PIPE).communicate()[0]
            if didAWin(output):
                score += 1
    return score

class Generation:
    def __init___(self, number=1, members=[]):
        self.number = number
        self.members = members
    def evaluateScores(self, environment):
        for entity in self.members:
            entity.score = calculateScore(entity, environment)
    def prepareNext(self, environment):
        self.members.sort(key=lambda x: x.score, reverse=True)
        nextGeneration = Generation(number+1, self.members[:5])
        for first_index in range(0,9):
            second_index = first_index
            while(second_index is first_index):
                second_index = randint(0,9)
            nextGeneration.members.append(environment.produceChild(self.members[first_index]),self.members[second_index])
        nextGeneration.members.append(environment.produceChild(self.members[4], self.members[5]))
        for counter in range(1,3):
            nextGeneration.members.append(environment.produceChild(self.members[0], self.members[1]))
            nextGeneration.members.append(environment.produceChild(self.members[2], self.members[3]))
        return nextGeneration
def loadResults(environment):
    if not os.path.exists('results/{}.xml'.format(environment.name)):
        return Generation()
    xml_tree = ET.parse('results/{}.xml'.format(environment.name))
    loadedEntites = []
    for xml_entity in tree.getroot().findall('entity'):
        score = xml_entity.attributes['score']
        variables = {}
        for xml_variable in xml_entity.find('variable'):
            variables[xml_variable.attributes['name']] = xml_variable.attributes['value']
        loadedEntities.append(EvolvingEntity(score, variables))
    return Generation(xml_tree.getroot().attributes['number'], loadedEntities).prepareNext()

def saveResults(environment, generation):
    #Create an empty file if doesn't exist
    open('results/{}.xml'.format(environment.name), 'a').close()
    xml_generation = ET.Element('generation')
    xml_generation.set('number', generation.number)
    for entity in generation.members:
        xml_entity = ET.subelement(xml_generation, 'entity')
        xml_entity.set('score', entity.score)
        for variable in entity.variables:
            xml_variable = ET.subelement(xml_entity, 'variable')
            xml_variable.set('name', variable)
            xml_variables.set('value', entity.variables[variable])
    xml_tree = ET.ElementTree(xml_generation)
    xml_tree.write('results/{}.xml'.format(environment.name))
 
environment = loadEnvironment(input('Which test environment do you want to run?\n'))
if not environment:
    print('Something went wrong! Cannot load the requested environment!')
    exit()
sys.stdout.write('Initializing genetic algorithm for environment {}...\n'.format(envirenment.name))
initConf()
generation = loadResults()
for number in range(len(generation.members), 19):
    generation.members.append(environment.generateEntity())
while True:
    sys.stdout.write('Testing generation {}... '.format(generation.number))
    generation.evaluateScores()
    sys.stdout.write('saving results... ')
    saveResults(envirenment, generation)
    sys.stdout.write('done!\n')
    generation = generation.prepareNext()
